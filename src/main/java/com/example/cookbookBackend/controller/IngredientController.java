package com.example.cookbookBackend.controller;

import com.example.cookbookBackend.controller.mapper.IngredientMapper;
import com.example.cookbookBackend.controller.messages.ingredientMessages.DeleteIngredientMessage;
import com.example.cookbookBackend.controller.messages.ingredientMessages.NewIngredientMessage;
import com.example.cookbookBackend.controller.messages.ingredientMessages.UpdateIngredientMessage;
import com.example.cookbookBackend.exception.CategoryNotFoundException;
import com.example.cookbookBackend.exception.IngredientNotFoundException;
import com.example.cookbookBackend.model.Ingredient;
import com.example.cookbookBackend.service.IngredientCategoryService;
import com.example.cookbookBackend.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("ingredients")
public class IngredientController {

    private final IngredientService ingredientService;
    private final IngredientCategoryService categoryService;
    private final IngredientMapper ingredientMapper;


    @Autowired
    public IngredientController(IngredientService ingredientService, IngredientCategoryService categoryService, IngredientMapper ingredientMapper) {
        this.ingredientService = ingredientService;
        this.categoryService = categoryService;
        this.ingredientMapper = ingredientMapper;
    }

    @GetMapping("by-id")
    private Mono<Ingredient> getById(@RequestParam String id) {
        return this.ingredientService.getById(id)
                .switchIfEmpty(Mono.error(new IngredientNotFoundException(HttpStatus.NOT_FOUND, "id", id)));
    }

    @GetMapping("name")
    private Mono<Ingredient> getByName(@RequestParam String name) {
        return this.ingredientService.getByName(name)
                .switchIfEmpty(Mono.error(new IngredientNotFoundException(HttpStatus.NOT_FOUND, "name", name)));
    }

    @GetMapping("category-id")
    private Flux<Ingredient> getByIngredientCategoryId(@RequestParam String categoryId) {
        return this.ingredientService.getByCategoryId(categoryId);
    }

    @GetMapping("all")
    private Flux<Ingredient> getAll() {
        return ingredientService.getAll();
    }

    @PostMapping("new")
    public Mono<Ingredient> saveIngredient(@RequestBody @Valid NewIngredientMessage message) {
        return this.categoryService.getById(message.getIngredientCategoryId())
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "id", message.getIngredientCategoryId())))
                .then(this.ingredientService.save(this.ingredientMapper.convertNewIngredient(message)));
    }

    @PutMapping("update")
    private Mono<Ingredient> update(@RequestBody @Valid UpdateIngredientMessage message) {
        if (message.getCategoryId() != null) {
            return this.categoryService.getById(message.getCategoryId())
                    .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "id", message.getCategoryId())))
                    .then(this.ingredientService.update(this.ingredientMapper.convertUpgradeIngredient(message)));
        }
        return this.ingredientService.update(this.ingredientMapper.convertUpgradeIngredient(message));
    }

    @DeleteMapping("delete")
    private Mono<?> delete(@RequestBody @Valid DeleteIngredientMessage message) {
        return this.ingredientService.delete(message.getId());
    }
}
