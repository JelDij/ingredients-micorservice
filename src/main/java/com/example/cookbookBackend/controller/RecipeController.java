package com.example.cookbookBackend.controller;

import com.example.cookbookBackend.controller.mapper.RecipeMapper;
import com.example.cookbookBackend.controller.messages.recipeMessages.DeleteRecipeMessage;
import com.example.cookbookBackend.controller.messages.recipeMessages.LikeMessage;
import com.example.cookbookBackend.controller.messages.recipeMessages.NewRecipeMessage;
import com.example.cookbookBackend.controller.messages.recipeMessages.UpdateRecipeMessage;
import com.example.cookbookBackend.exception.*;
import com.example.cookbookBackend.model.Recipe;
import com.example.cookbookBackend.model.RecipeCategory;
import com.example.cookbookBackend.service.IngredientService;
import com.example.cookbookBackend.service.RecipeCategoryService;
import com.example.cookbookBackend.service.RecipeService;
import com.example.cookbookBackend.service.RecipeStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("recipe")
public class RecipeController {

    private final RecipeService recipeService;
    private final IngredientService ingredientService;
    private final RecipeCategoryService recipeCategoryService;
    private final RecipeStepService recipeStepService;
    private final RecipeMapper recipeMapper;

    @Autowired
    public RecipeController(RecipeService recipeService, IngredientService ingredientService, RecipeCategoryService recipeCategoryService, RecipeStepService recipeStepService, RecipeMapper recipeMapper) {
        this.recipeService = recipeService;
        this.ingredientService = ingredientService;
        this.recipeCategoryService = recipeCategoryService;
        this.recipeStepService = recipeStepService;
        this.recipeMapper = recipeMapper;
    }

    @GetMapping("by-id")
    private Mono<Recipe> getById(@RequestParam String id) {
        return this.recipeService.getById(id)
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.NOT_FOUND, id)));
    }

    @GetMapping("all")
    private Flux<Recipe> getAll() {
        return this.recipeService.getAll();
    }

    @GetMapping("by-creator-id")
    private Flux<Recipe> getByCreatorId(String id) {
        // TODO: check if creatorId exists (wait for extra microservice to implement this)
        return this.recipeService.getByCreatorId(id);
    }

    @GetMapping("by-ingredient-id")
    private Flux<Recipe> getByIngredientId(String id) {
        // TODO: implement
        return null;
    }

    @GetMapping("by-portions-exact")
    private Flux<Recipe> getByPortions(Double portions) {
        return this.recipeService.getByPortionsExact(portions);
    }

    @GetMapping("by-portions-min")
    private Flux<Recipe> getByMinPortions(Double portions) {
        return this.recipeService.getByPortionsGreaterThan(portions);
    }

    @GetMapping("by-portions-max")
    private Flux<Recipe> getByMaxPortions(Double portions) {
        return this.recipeService.getByPortionsLessThan(portions);
    }

    @GetMapping("by-likes-exact")
    private Flux<Recipe> getByLikes(int likes) {
        return this.recipeService.getByExactNumberOfLikes(likes);
    }

    @GetMapping("by-likes-min")
    private Flux<Recipe> getByMinLinkes(int likes) {
        return this.recipeService.getByMinNumberOfLikes(likes);
    }

    @GetMapping("by-likes-max")
    private Flux<Recipe> getByMaxLikes(int likes) {
        return this.recipeService.getByMaxNumberOfLikes(likes);
    }

    @PostMapping("new")
    private Mono<Recipe> saveRecipe(@RequestBody @Valid NewRecipeMessage message) {
        // TODO check if user exists
        return this.checkForValidCategory(message.getRecipeCategoryId())
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "categoryId", message.getRecipeCategoryId())))
                .then(this.checkForValidIngredients(message.getIngredients()))
                .flatMap(this::checkIngredientBoolean)
                .then(this.recipeService.save(this.recipeMapper.convertNewRecipe(message)));
    }

    @PutMapping("update")
    private Mono<Recipe> update(@RequestBody @Valid UpdateRecipeMessage message) {
        Mono<Boolean> categoryCheck =  Mono.just(true);
        Mono<Boolean> ingredientCheck = Mono.just(true);

        // TODO: Implment user check as well!
        if (message.getRecipeCategoryId() != null) {
            categoryCheck = this.checkForValidCategory(message.getRecipeCategoryId())
                    .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "categoryId", message.getRecipeCategoryId())))
                    .thenReturn(true);
        }

        if (message.getIngredients().size() > 0) {
            ingredientCheck = this.checkForValidIngredients(message.getIngredients())
                    .flatMap(this::checkIngredientBoolean);
        }
        return Mono.zip(categoryCheck, ingredientCheck)
                .flatMap(tuple -> this.recipeService.update(this.recipeMapper.convertUpdateRecipe(message)));
    }

    private Mono<Boolean> checkForValidUser(String userId) {
        // TODO: implement
        return Mono.just(Boolean.TRUE);
    }

    private Mono<RecipeCategory> checkForValidCategory(String categoryId) {
        return this.recipeCategoryService.getById(categoryId);
    }
    //
    private Mono<Boolean> checkForValidIngredients(Map<String, Double> ingredients) {
        return Flux.fromIterable(ingredients.keySet())
                .flatMap(this.ingredientService::getById)
                .collectList()
                .filter(Objects::nonNull)
                .map(validIngredients -> validIngredients.size() == ingredients.size());
    }

    private Mono<Boolean> checkIngredientBoolean(Boolean ingredientsAreValid) {
        if (!ingredientsAreValid) {
            return Mono.error(new IngredientsInvalidException());
        }
        return Mono.just(true);
    }

    @PutMapping("add-like")
    private Mono<Recipe> addLike(@RequestBody @Valid LikeMessage message) {
        return this.recipeService.addLike(message.getUserId(), message.getRecipeId());
    }

    @PutMapping("remove-like")
    private Mono<Recipe> removeLike(@RequestBody @Valid LikeMessage message) {
        return this.recipeService.removeLike(message.getUserId(), message.getRecipeId());
    }

    @DeleteMapping("delete")
    private Mono<?> delete(@RequestBody @Valid DeleteRecipeMessage message) {
        return this.recipeService.getById(message.getId())
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.BAD_REQUEST, message.getId())))
                .flatMapMany(recipe -> this.recipeStepService.getByRecipeId(message.getId()))
                .flatMap(recipeSteps -> this.recipeStepService.delete(recipeSteps.getId()))
                .then(this.recipeService.delete(message.getId()));
    }
}
