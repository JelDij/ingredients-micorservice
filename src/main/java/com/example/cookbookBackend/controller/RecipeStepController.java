package com.example.cookbookBackend.controller;

import com.example.cookbookBackend.controller.mapper.RecipeStepMapper;
import com.example.cookbookBackend.controller.messages.recipeStepMessages.DeleteRecipeStepMessage;
import com.example.cookbookBackend.controller.messages.recipeStepMessages.NewRecipeStepMessage;
import com.example.cookbookBackend.controller.messages.recipeStepMessages.UpdateRecipeStepMessage;
import com.example.cookbookBackend.exception.RecipeIdNotFound;
import com.example.cookbookBackend.exception.RecipeNotFoundException;
import com.example.cookbookBackend.exception.RecipeStepNotFoundException;
import com.example.cookbookBackend.model.Recipe;
import com.example.cookbookBackend.model.RecipeStep;
import com.example.cookbookBackend.service.RecipeService;
import com.example.cookbookBackend.service.RecipeStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("recipe-steps")
public class RecipeStepController {

    private final RecipeStepService recipeStepService;

    private final RecipeService recipeService;

    private final RecipeStepMapper recipeStepMapper;

    @Autowired
    public RecipeStepController(RecipeStepService recipeStepService, RecipeService recipeService, RecipeStepMapper recipeStepMapper) {
        this.recipeStepService = recipeStepService;
        this.recipeService = recipeService;
        this.recipeStepMapper = recipeStepMapper;
    }

    @GetMapping("by-id")
    private Mono<RecipeStep> getById(@RequestParam String id) {
        return this.recipeStepService.getById(id)
                .switchIfEmpty(Mono.error(new RecipeStepNotFoundException(id)));
    }

    @GetMapping("all")
    private Flux<RecipeStep> getAll() {
        return this.recipeStepService.getAll();
    }

    @GetMapping("by-recipe-id")
    private Flux<RecipeStep> getByRecipeId(@RequestParam String id) {
        return this.recipeStepService.getByRecipeId(id)
                .switchIfEmpty(Mono.error(new RecipeIdNotFound(id)));
    }

    @PostMapping("new")
    private Mono<RecipeStep> saveRecipeStep(@RequestBody @Valid NewRecipeStepMessage message) {
        return this.recipeService.getById(message.getRecipeId())
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.BAD_REQUEST, message.getRecipeId())))
                .then(this.recipeStepService.save(this.recipeStepMapper.convertNewRecipeStep(message)));
    }


    @PutMapping("update")
    private Mono<RecipeStep> updateRecipeStep(@RequestBody @Valid UpdateRecipeStepMessage message) {
        return this.recipeStepService.update(this.recipeStepMapper.convertUpgradeRecipeStep(message));
    }

    @DeleteMapping("delete")
    private Mono<?> delete(@RequestBody @Valid DeleteRecipeStepMessage message) {
        return this.recipeStepService.delete(message.getId());
    }
}
