package com.example.cookbookBackend.controller;

import com.example.cookbookBackend.controller.messages.recipeMessages.NewRecipeMessage;
import com.example.cookbookBackend.model.Recipe;
import com.example.cookbookBackend.service.IngredientCategoryService;
import com.example.cookbookBackend.service.IngredientService;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

@RestController
@RequestMapping("csv")
public class CsvController {

    private final IngredientCategoryService ingredientCategoryService;
    private final IngredientService ingredientService;
    @Autowired
    public CsvController(IngredientCategoryService ingredientCategoryService, IngredientService ingredientService) {
        this.ingredientCategoryService = ingredientCategoryService;
        this.ingredientService = ingredientService;
    }

    @PostMapping("ingredients")
    private Flux<Object> readFromCsvFile(@RequestPart("file") FilePart file) {
        System.out.println(file);
        file.content().map(dataBuffer -> {
            dataBuffer.asInputStream()
        })
        return Flux.empty();
    }
}

