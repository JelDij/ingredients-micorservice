package com.example.cookbookBackend.controller.messages.recipeMessages;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

public class NewRecipeMessage {

    @NotEmpty(message = "{name.empty}")
    private String name;

    @NotEmpty(message = "{creator.id.empty}")
    private String creatorId;

    private Map<String, Double> ingredients;

    @Min(value = 1, message = "{portions.minimum}")
    private Double portions;

    @NotEmpty(message = "{recipe.category.id.empty}")
    private String recipeCategoryId;

    private String image;

    @NotEmpty(message = "{description.empty}")
    private String description;

    @Min(value = 1, message = "{cook.time.minimum}")
    private Double cookTimeInMinutes;

    public NewRecipeMessage() {
        this.ingredients = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public Map<String, Double> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<String, Double> ingredients) {
        this.ingredients = ingredients;
    }

    public Double getPortions() {
        return portions;
    }

    public void setPortions(Double portions) {
        this.portions = portions;
    }

    public String getRecipeCategoryId() {
        return recipeCategoryId;
    }

    public void setRecipeCategoryId(String recipeCategoryId) {
        this.recipeCategoryId = recipeCategoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getCookTimeInMinutes() {
        return cookTimeInMinutes;
    }

    public void setCookTimeInMinutes(Double cookTimeInMinutes) {
        this.cookTimeInMinutes = cookTimeInMinutes;
    }
}
