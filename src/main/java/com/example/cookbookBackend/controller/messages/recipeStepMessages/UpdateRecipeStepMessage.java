package com.example.cookbookBackend.controller.messages.recipeStepMessages;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class UpdateRecipeStepMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    @Min(value = 1L, message = "{stepnumber.mandatory}")
    private Integer stepNumber;

    private String instruction;


    public UpdateRecipeStepMessage() {
    }

    public String getId() {
        return id;
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

}
