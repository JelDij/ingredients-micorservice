package com.example.cookbookBackend.controller.messages.recipeMessages;

import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

public class UpdateRecipeMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    private String name;

    private Map<String, Double> ingredients;

    private Double portions;

    private String recipeCategoryId;

    private String image;

    private String description;

    private Double cookTimeInMinutes;

    public UpdateRecipeMessage() {
        this.ingredients = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Double> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<String, Double> ingredients) {
        this.ingredients = ingredients;
    }

    public Double getPortions() {
        return portions;
    }

    public void setPortions(Double portions) {
        this.portions = portions;
    }

    public String getRecipeCategoryId() {
        return recipeCategoryId;
    }

    public void setRecipeCategoryId(String recipeCategoryId) {
        this.recipeCategoryId = recipeCategoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getCookTimeInMinutes() {
        return cookTimeInMinutes;
    }

    public void setCookTimeInMinutes(Double cookTimeInMinutes) {
        this.cookTimeInMinutes = cookTimeInMinutes;
    }
}
