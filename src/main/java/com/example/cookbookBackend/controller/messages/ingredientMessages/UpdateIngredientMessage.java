package com.example.cookbookBackend.controller.messages.ingredientMessages;

import javax.validation.constraints.NotEmpty;

public class UpdateIngredientMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;
    private String name;
    private String categoryId;
    private String unitOfMeasurement;

    public UpdateIngredientMessage() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }
}
