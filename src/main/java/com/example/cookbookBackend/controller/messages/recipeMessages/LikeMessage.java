package com.example.cookbookBackend.controller.messages.recipeMessages;

import javax.validation.constraints.NotEmpty;

public class LikeMessage {

    @NotEmpty(message = "{user.id.empty}")
    private String userId;
    @NotEmpty(message = "{recipe.id.empty}")
    private String recipeId;

    public LikeMessage() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }
}
