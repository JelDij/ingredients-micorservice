package com.example.cookbookBackend.controller.messages.recipeCategoryMessages;

import javax.validation.constraints.NotEmpty;

public class DeleteRecipeCategoryMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    public DeleteRecipeCategoryMessage() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
