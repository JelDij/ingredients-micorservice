package com.example.cookbookBackend.controller.messages.recipeCategoryMessages;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UpdateRecipeCategoryMessage {

    @NotNull(message = "{id.empty}")
    private String id;
    @NotEmpty(message = "{name.empty}")
    private String name;

    public UpdateRecipeCategoryMessage() {}


    public String getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
