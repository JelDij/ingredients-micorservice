package com.example.cookbookBackend.controller.messages.recipeCategoryMessages;

import javax.validation.constraints.NotEmpty;

public class NewRecipeCategoryMessage {

    @NotEmpty(message = "{name.empty}")
    private String name;

    public NewRecipeCategoryMessage() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
