package com.example.cookbookBackend.controller.messages.ingredientMessages;

import javax.validation.constraints.NotEmpty;

public class NewIngredientMessage {

    @NotEmpty(message = "{name.empty}")
    private String name;
    @NotEmpty(message = "{ingredientCategory.id.empty}")
    private String ingredientCategoryId;
    @NotEmpty(message = "{unitOfMeasurement.empty}")
    private String unitOfMeasurement;

    public NewIngredientMessage() {
    }

    public String getName() {
        return name;
    }

    public String getIngredientCategoryId() {
        return ingredientCategoryId;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIngredientCategoryId(String ingredientCategoryId) {
        this.ingredientCategoryId = ingredientCategoryId;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }
}
