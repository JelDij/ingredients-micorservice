package com.example.cookbookBackend.controller.messages.ingredientCategoryMessages;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UpdateIngredientCategoryMessage {

    @NotNull(message = "{id.empty}")
    private String id;
    @NotEmpty(message = "{name.empty}")
    private String name;

    public UpdateIngredientCategoryMessage() {    }

    public String getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
