package com.example.cookbookBackend.controller.messages.ingredientMessages;

import javax.validation.constraints.NotEmpty;

public class DeleteIngredientMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    public DeleteIngredientMessage() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
