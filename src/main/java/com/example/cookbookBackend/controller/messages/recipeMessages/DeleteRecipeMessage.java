package com.example.cookbookBackend.controller.messages.recipeMessages;

import javax.validation.constraints.NotEmpty;

public class DeleteRecipeMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    public DeleteRecipeMessage() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
