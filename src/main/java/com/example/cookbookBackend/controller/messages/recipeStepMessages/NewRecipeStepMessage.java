package com.example.cookbookBackend.controller.messages.recipeStepMessages;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class NewRecipeStepMessage {
    @Min(value = 1L, message = "{stepnumber.mandatory}")
    private Integer stepNumber;

    @NotEmpty(message = "{recipeStep.instruction.empty}")
    private String instruction;

    @NotEmpty(message = "{recipeStep.recipeId.empty}")
    private String recipeId;

    public NewRecipeStepMessage() {
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public String getInstruction() {
        return instruction;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }
}
