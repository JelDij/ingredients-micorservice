package com.example.cookbookBackend.controller.messages.ingredientCategoryMessages;

import javax.validation.constraints.NotEmpty;

public class NewIngredientCategoryMessage {

    @NotEmpty(message = "{name.empty}")
    private String name;

    public NewIngredientCategoryMessage() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
