package com.example.cookbookBackend.controller.messages.recipeStepMessages;

import javax.validation.constraints.NotEmpty;

public class DeleteRecipeStepMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    public DeleteRecipeStepMessage() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
