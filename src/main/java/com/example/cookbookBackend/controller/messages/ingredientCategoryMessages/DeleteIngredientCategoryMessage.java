package com.example.cookbookBackend.controller.messages.ingredientCategoryMessages;

import javax.validation.constraints.NotEmpty;

public class DeleteIngredientCategoryMessage {

    @NotEmpty(message = "{id.empty}")
    private String id;

    public DeleteIngredientCategoryMessage() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
