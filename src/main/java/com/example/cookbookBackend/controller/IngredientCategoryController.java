package com.example.cookbookBackend.controller;

import com.example.cookbookBackend.controller.mapper.IngredientCategoryMapper;
import com.example.cookbookBackend.controller.messages.ingredientCategoryMessages.DeleteIngredientCategoryMessage;
import com.example.cookbookBackend.controller.messages.ingredientCategoryMessages.NewIngredientCategoryMessage;
import com.example.cookbookBackend.controller.messages.ingredientCategoryMessages.UpdateIngredientCategoryMessage;
import com.example.cookbookBackend.exception.CategoryInUseException;
import com.example.cookbookBackend.exception.CategoryNotFoundException;
import com.example.cookbookBackend.model.IngredientCategory;
import com.example.cookbookBackend.service.IngredientCategoryService;
import com.example.cookbookBackend.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("ingredient-categories")
public class IngredientCategoryController {

    private final IngredientCategoryService ingredientCategoryService;
    private final IngredientService ingredientService;
    private final IngredientCategoryMapper ingredientCategoryMapper;

    @Autowired
    public IngredientCategoryController(IngredientCategoryService IngredientCategoryService, IngredientService ingredientService, IngredientCategoryMapper ingredientCategoryMapper) {
        this.ingredientCategoryService = IngredientCategoryService;
        this.ingredientService = ingredientService;
        this.ingredientCategoryMapper = ingredientCategoryMapper;
    }

    @GetMapping("by-id")
    private Mono<IngredientCategory> getById(@RequestParam String id) {
        return this.ingredientCategoryService.getById(id)
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.NOT_FOUND, "id", id)));
    }

    @GetMapping("name")
    private Mono<IngredientCategory> getByName(@RequestParam String name) {
        return this.ingredientCategoryService.getByName(name)
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.NOT_FOUND, "name", name)));
    }

    @GetMapping("all")
    private Flux<IngredientCategory> getAll() {
        return ingredientCategoryService.getAll();
    }

    @PostMapping("new")
    private Mono<?> save(@RequestBody @Valid NewIngredientCategoryMessage message) {
        return this.ingredientCategoryService.save(this.ingredientCategoryMapper.convertNewIngredientCategory(message));
    }

    @PutMapping("update")
    private Mono<IngredientCategory> update(@RequestBody @Valid UpdateIngredientCategoryMessage message) {
        return this.ingredientCategoryService.update(this.ingredientCategoryMapper.convertUpdateIngredientCategory(message));
    }

    @DeleteMapping("delete")
    private Mono<?> delete(@RequestBody @Valid DeleteIngredientCategoryMessage message) {
        return this.ingredientService.getByCategoryId(message.getId())
                .count()
                .flatMap(count -> {
                    if (count == 0){
                        return this.ingredientCategoryService.delete(message.getId());
                    } else {
                        return Mono.error(new CategoryInUseException(message.getId()));
                    }
                });
    }
}
