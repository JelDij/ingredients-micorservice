package com.example.cookbookBackend.controller.mapper;

import com.example.cookbookBackend.controller.messages.ingredientCategoryMessages.NewIngredientCategoryMessage;
import com.example.cookbookBackend.controller.messages.ingredientCategoryMessages.UpdateIngredientCategoryMessage;
import com.example.cookbookBackend.model.IngredientCategory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IngredientCategoryMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public IngredientCategoryMapper(ModelMapper mapper) {
        this.modelMapper = mapper;
    }

    /**
     * Very simple mapper which doesn't make use of webflux because IngredientCategory has no special fields and the json can be
     * mapped directly
     * @param message to create category
     * @return IngredientCategoryCategory
     */
    public IngredientCategory convertNewIngredientCategory(NewIngredientCategoryMessage message) {
        return this.modelMapper.map(message, IngredientCategory.class);
    }

    /**
     * Mapper to update a category
     * @param message to update a category
     * @return IngredientCategory
     */
    public IngredientCategory convertUpdateIngredientCategory(UpdateIngredientCategoryMessage message) {
        return this.modelMapper.map(message, IngredientCategory.class);
    }
}
