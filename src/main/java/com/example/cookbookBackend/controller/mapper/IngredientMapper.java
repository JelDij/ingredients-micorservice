package com.example.cookbookBackend.controller.mapper;

import com.example.cookbookBackend.controller.messages.ingredientMessages.NewIngredientMessage;
import com.example.cookbookBackend.controller.messages.ingredientMessages.UpdateIngredientMessage;
import com.example.cookbookBackend.model.Ingredient;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IngredientMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public IngredientMapper(ModelMapper mapper) {
        this.modelMapper = mapper;
    }

    /**
     * converts NewIngredientMessage to Ingredient object. We don't set the id because we don't have an ingredient id yet
     * @param message to create new ingredient
     * @return Mono with ingredient object
     */
    public Ingredient convertNewIngredient(NewIngredientMessage message) {
        // we skip the setId because otherwise CategoryID will be set as the id of the object
        return this.modelMapper.typeMap(NewIngredientMessage.class, Ingredient.class)
                .addMappings(mapper -> mapper.skip(Ingredient::setId))
                .map(message);
    }

    public Ingredient convertUpgradeIngredient(UpdateIngredientMessage message) {
        return this.modelMapper.map(message, Ingredient.class);
    }
}
