package com.example.cookbookBackend.controller.mapper;

import com.example.cookbookBackend.controller.messages.recipeMessages.NewRecipeMessage;
import com.example.cookbookBackend.controller.messages.recipeMessages.UpdateRecipeMessage;
import com.example.cookbookBackend.model.Recipe;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecipeMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public RecipeMapper(ModelMapper mapper) {
        this.modelMapper = mapper;
    }

    public Recipe convertNewRecipe(NewRecipeMessage message) {
        return this.modelMapper.typeMap(NewRecipeMessage.class, Recipe.class)
                .addMappings(mapper -> mapper.skip(Recipe::setId))
                .map(message);
    }

    public Recipe convertUpdateRecipe(UpdateRecipeMessage message) {
        return this.modelMapper.map(message, Recipe.class);
    }
}
