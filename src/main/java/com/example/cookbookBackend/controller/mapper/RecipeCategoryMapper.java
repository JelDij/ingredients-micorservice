package com.example.cookbookBackend.controller.mapper;

import com.example.cookbookBackend.controller.messages.recipeCategoryMessages.NewRecipeCategoryMessage;
import com.example.cookbookBackend.controller.messages.recipeCategoryMessages.UpdateRecipeCategoryMessage;
import com.example.cookbookBackend.model.RecipeCategory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecipeCategoryMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public RecipeCategoryMapper(ModelMapper mapper) {
        this.modelMapper = mapper;
    }

    /**
     * Very simple mapper which doesn't make use of webflux because RecipeCategory has no special fields and the json can be
     * mapped directly
     * @param message to create category
     * @return RecipeCategory
     */
    public RecipeCategory convertNewRecipeCategory(NewRecipeCategoryMessage message) {
        return this.modelMapper.map(message, RecipeCategory.class);
    }

    /**
     * Mapper to update a category
     * @param message to update a category
     * @return IngredientCategory
     */
    public RecipeCategory convertUpdateRecipeCategory(UpdateRecipeCategoryMessage message) {
        return this.modelMapper.map(message, RecipeCategory.class);
    }

}
