package com.example.cookbookBackend.controller.mapper;

import com.example.cookbookBackend.controller.messages.recipeStepMessages.NewRecipeStepMessage;
import com.example.cookbookBackend.controller.messages.recipeStepMessages.UpdateRecipeStepMessage;
import com.example.cookbookBackend.model.RecipeStep;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecipeStepMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public RecipeStepMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public RecipeStep convertNewRecipeStep(NewRecipeStepMessage message) {
        return this.modelMapper.typeMap(NewRecipeStepMessage.class, RecipeStep.class)
                .addMappings(mapper -> mapper.skip(RecipeStep::setId))
                .map(message);
    }

    public RecipeStep convertUpgradeRecipeStep(UpdateRecipeStepMessage message) {
        return this.modelMapper.map(message, RecipeStep.class);
    }

}
