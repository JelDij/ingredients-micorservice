package com.example.cookbookBackend.controller;

import com.example.cookbookBackend.controller.mapper.RecipeCategoryMapper;
import com.example.cookbookBackend.controller.messages.ingredientCategoryMessages.DeleteIngredientCategoryMessage;
import com.example.cookbookBackend.controller.messages.recipeCategoryMessages.NewRecipeCategoryMessage;
import com.example.cookbookBackend.controller.messages.recipeCategoryMessages.UpdateRecipeCategoryMessage;
import com.example.cookbookBackend.exception.CategoryNotFoundException;
import com.example.cookbookBackend.model.RecipeCategory;
import com.example.cookbookBackend.service.RecipeCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("recipe-categories")
public class RecipeCategoryController {

    private final RecipeCategoryService recipeCategoryService;

    private final RecipeCategoryMapper recipeCategoryMapper;

    @Autowired
    public RecipeCategoryController(RecipeCategoryService recipeCategoryService, RecipeCategoryMapper recipeCategoryMapper) {
        this.recipeCategoryService = recipeCategoryService;
        this.recipeCategoryMapper = recipeCategoryMapper;
    }

    @GetMapping("by-id")
    private Mono<RecipeCategory> getById(@RequestParam String id) {
        return this.recipeCategoryService.getById(id)
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.NOT_FOUND, "id", id)));
    }

    @GetMapping("name")
    private Mono<RecipeCategory> getByName(@RequestParam String name) {
        return this.recipeCategoryService.getByName(name)
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.NOT_FOUND, "name", name)));
    }

    @GetMapping("all")
    private Flux<RecipeCategory> getAll() {
        return recipeCategoryService.getAll();
    }

    @PostMapping("new")
    private Mono<?> save(@RequestBody @Valid NewRecipeCategoryMessage message) {
        return this.recipeCategoryService.save(this.recipeCategoryMapper.convertNewRecipeCategory(message));
    }

    @PutMapping("update")
    private Mono<RecipeCategory> update(@RequestBody @Valid UpdateRecipeCategoryMessage message) {
        return this.recipeCategoryService.update(this.recipeCategoryMapper.convertUpdateRecipeCategory(message));
    }

    @DeleteMapping("delete")
    private Mono<?> delete(@RequestBody @Valid DeleteIngredientCategoryMessage message) {
        // TODO: if recipes are added, check if category is used in a recipe
        return this.recipeCategoryService.delete(message.getId());
    }
}
