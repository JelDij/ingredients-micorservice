package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IngredientNotFoundException extends ResponseStatusException {

    public IngredientNotFoundException(HttpStatus status, String searchTerm, String searchParameter) {
        super(status, String.format("No ingredient found with %s %s", searchTerm, searchParameter));
    }
}
