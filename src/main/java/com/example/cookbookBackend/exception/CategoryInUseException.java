package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CategoryInUseException extends ResponseStatusException {

    public CategoryInUseException(String id) {
        super(HttpStatus.CONFLICT, String.format("Can't delete category with id %s, it is used in recipes", id));
    }
}
