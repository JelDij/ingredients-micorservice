package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeInvalidException extends ResponseStatusException {
    public RecipeInvalidException() {
        super(HttpStatus.BAD_REQUEST, String.format("Recipe could not be created. Please check the provided input"));
    }
}
