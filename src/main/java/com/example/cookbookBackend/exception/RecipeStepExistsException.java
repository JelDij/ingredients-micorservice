package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeStepExistsException extends ResponseStatusException {

    public RecipeStepExistsException(String id) {
        super(HttpStatus.CONFLICT, String.format("Recipe step with id %s already exists", id));
    }
}
