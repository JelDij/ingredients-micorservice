package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeExistsException extends ResponseStatusException {


    public RecipeExistsException(String name) {
        super(HttpStatus.BAD_REQUEST, String.format("Recipe with name %s already exists", name));
    }
}
