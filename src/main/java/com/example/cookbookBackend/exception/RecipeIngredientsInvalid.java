package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeIngredientsInvalid  extends ResponseStatusException {

    public RecipeIngredientsInvalid() {
        super(HttpStatus.BAD_REQUEST, String.format("Recipe with contains ingredients that do not exist"));
    }
}
