package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeNotFoundException extends ResponseStatusException {

    public RecipeNotFoundException(HttpStatus status, String id) {
        super(status, String.format("Recipe with id %s not found", id));
    }
}
