package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeIdNotFound extends ResponseStatusException {

    public RecipeIdNotFound(String id) {
        super(HttpStatus.NOT_FOUND, String.format("Recipe step with id %s not found", id));

    }
}
