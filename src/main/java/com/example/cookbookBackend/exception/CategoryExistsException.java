package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CategoryExistsException extends ResponseStatusException {

    public CategoryExistsException(String name) {
        super(HttpStatus.CONFLICT, String.format("Category with name %s already exists", name));
    }


}
