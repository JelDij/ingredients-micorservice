package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RecipeStepNotFoundException extends ResponseStatusException {

    public RecipeStepNotFoundException(String id) {
        super(HttpStatus.NOT_FOUND, String.format("Recipe step with id %s not found", id));
    }
}
