package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CategoryNotFoundException extends ResponseStatusException {

    public CategoryNotFoundException(HttpStatus status, String searchTerm, String searchParameter) {
        super(status, String.format("No category found with %s %s", searchTerm, searchParameter));
    }
}
