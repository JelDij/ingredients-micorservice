package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IngredientsInvalidException extends ResponseStatusException {

    public IngredientsInvalidException() {
        super(HttpStatus.BAD_REQUEST, "Some or all of the provided ingredients are invalid or don't yet exists. Therefore the recipe cannot be created");

    }
}
