package com.example.cookbookBackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IngredientExistsException extends ResponseStatusException {

    public IngredientExistsException(String name) {
        super(HttpStatus.CONFLICT, String.format("Ingredient with name %s already exists", name));
    }
}
