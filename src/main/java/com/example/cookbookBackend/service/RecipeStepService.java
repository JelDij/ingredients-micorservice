package com.example.cookbookBackend.service;

import com.example.cookbookBackend.model.RecipeStep;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RecipeStepService {

    Mono<RecipeStep> getById(String id);

    Flux<RecipeStep> getAll();

    Flux<RecipeStep> getByRecipeId(String recipeId);

    Mono<RecipeStep> save(RecipeStep recipeStep);

    Mono<RecipeStep> update(RecipeStep recipeStep);

    Mono<?> delete(String id);
}
