package com.example.cookbookBackend.service;

import com.example.cookbookBackend.model.RecipeCategory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RecipeCategoryService {

    Mono<RecipeCategory> getById(String id);

    Mono<RecipeCategory> getByName(String name);

    Flux<RecipeCategory> getAll();

    Mono<RecipeCategory> save(RecipeCategory recipeCategory);

    Mono<RecipeCategory> update(RecipeCategory recipeCategory);

    Mono<?> delete(String id);
}
