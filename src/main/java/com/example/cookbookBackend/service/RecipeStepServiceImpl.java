package com.example.cookbookBackend.service;

import com.example.cookbookBackend.exception.RecipeStepExistsException;
import com.example.cookbookBackend.exception.RecipeStepNotFoundException;
import com.example.cookbookBackend.model.RecipeStep;
import com.example.cookbookBackend.repository.RecipeStepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RecipeStepServiceImpl implements RecipeStepService {

    private final RecipeStepRepository recipeStepRepository;

    @Autowired
    public RecipeStepServiceImpl(RecipeStepRepository recipeStepRepository) {
        this.recipeStepRepository = recipeStepRepository;
    }

    @Override
    public Mono<RecipeStep> getById(String id) {
        return this.recipeStepRepository.findById(id);
    }

    @Override
    public Flux<RecipeStep> getAll() {
        return this.recipeStepRepository.findAll();
    }

    @Override
    public Flux<RecipeStep> getByRecipeId(String recipeId) {
        return this.recipeStepRepository.findRecipeStepsByRecipeId(recipeId);
    }

    @Override
    public Mono<RecipeStep> save(RecipeStep recipeStep) {
        return this.recipeStepRepository.save(recipeStep)
                .onErrorResume(e -> Mono.error(new RecipeStepExistsException(recipeStep.getRecipeId())));
    }

    @Override
    public Mono<RecipeStep> update(RecipeStep recipeStep) {
        return this.recipeStepRepository.findById(recipeStep.getId())
                .switchIfEmpty(Mono.error(new RecipeStepNotFoundException(recipeStep.getId())))
                .flatMap(recipeStepToUpgrade -> updateFields(recipeStep, recipeStepToUpgrade))
                .flatMap(this.recipeStepRepository::save);
    }

    private Mono<RecipeStep> updateFields(RecipeStep recipeStep, RecipeStep recipeToUpgrade) {
        if (!recipeStep.getStepNumber().equals(recipeToUpgrade.getStepNumber())) {
            recipeToUpgrade.setStepNumber(recipeStep.getStepNumber());
        }
        if (recipeStep.getInstruction() != null) {
            if (!recipeStep.getInstruction().equals(recipeToUpgrade.getInstruction())) {
                recipeToUpgrade.setInstruction(recipeStep.getInstruction());
            }
        }
        return Mono.just(recipeToUpgrade);
    }

    @Override
    public Mono<?> delete(String id) {
        return this.recipeStepRepository.findById(id)
                .switchIfEmpty(Mono.error(new RecipeStepNotFoundException(id)))
                .flatMap(this.recipeStepRepository::delete);
    }
}
