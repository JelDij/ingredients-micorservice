package com.example.cookbookBackend.service;

import com.example.cookbookBackend.model.Ingredient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IngredientService {

    Mono<Ingredient> getById(String id);

    Mono<Ingredient> getByName(String name);

    Flux<Ingredient> getAll();

    Flux<Ingredient> getByCategoryId(String categoryId);

    Mono<Ingredient> save(Ingredient ingredient);

    Mono<Ingredient> update(Ingredient ingredient);

    Mono<?> delete(String id);
    
}
