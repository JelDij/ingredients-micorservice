package com.example.cookbookBackend.service;

import com.example.cookbookBackend.exception.IngredientExistsException;
import com.example.cookbookBackend.exception.IngredientNotFoundException;
import com.example.cookbookBackend.model.Ingredient;
import com.example.cookbookBackend.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class IngredientServiceImpl implements IngredientService {

    private final IngredientRepository ingredientRepository;

    @Autowired
    public IngredientServiceImpl(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public Mono<Ingredient> getById(String id) {
        return this.ingredientRepository.findById(id);
    }

    @Override
    public Mono<Ingredient> getByName(String name) {
        return this.ingredientRepository.findByNameIgnoreCase(name);
    }

    @Override
    public Flux<Ingredient> getAll() {
        return this.ingredientRepository.findAll();
    }

    @Override
    public Flux<Ingredient> getByCategoryId(String categoryId) {
        return this.ingredientRepository.findIngredientByIngredientCategoryId(categoryId);
    }

    @Override
    public Mono<Ingredient> save(Ingredient ingredient) {
        return this.ingredientRepository.save(ingredient)
                .onErrorResume(e -> Mono.error(new IngredientExistsException(ingredient.getName())));
    }

    @Override
    public Mono<Ingredient> update(Ingredient ingredient) {
        return ingredientRepository.findById(ingredient.getId())
                .switchIfEmpty(Mono.error(new IngredientNotFoundException(HttpStatus.BAD_REQUEST, "id", ingredient.getId())))
                .flatMap(ingredientToUpdate -> updateFields(ingredient, ingredientToUpdate))
                .flatMap(this.ingredientRepository::save);
    }

    private Mono<Ingredient> updateFields(Ingredient ingredient, Ingredient ingredientToUpdate) {
        if (ingredient.getName() != null) {
            if (!ingredient.getName().equals(ingredientToUpdate.getName())) {
                ingredientToUpdate.setName(ingredient.getName());
            }
        }
        if (ingredient.getIngredientCategoryId() != null) {
            if (!ingredient.getIngredientCategoryId().equals(ingredientToUpdate.getIngredientCategoryId())) {
                ingredientToUpdate.setIngredientCategoryId(ingredient.getIngredientCategoryId());
            }
        }
        if (ingredient.getUnitOfMeasurement() != null) {
            if (!ingredient.getUnitOfMeasurement().equals(ingredientToUpdate.getUnitOfMeasurement())) {
                ingredientToUpdate.setUnitOfMeasurement(ingredient.getUnitOfMeasurement());
            }
        }
        return Mono.just(ingredientToUpdate);
    }

    @Override
    public Mono<?> delete(String id) {
        return this.ingredientRepository.findById(id)
                .switchIfEmpty(Mono.error(new IngredientNotFoundException(HttpStatus.BAD_REQUEST, "id", id)))
                .flatMap(this.ingredientRepository::delete);
    }
}
