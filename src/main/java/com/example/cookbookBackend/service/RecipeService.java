package com.example.cookbookBackend.service;

import com.example.cookbookBackend.model.Recipe;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RecipeService {

    Mono<Recipe> getById(String id);

    Mono<Recipe> getByName(String name);

    Flux<Recipe> getAll();

    Flux<Recipe> getByCreatorId(String creatorId);

    Flux<Recipe> getByIngredientsId(String ingredientId);

    Flux<Recipe> getByPortionsExact(Double portions);

    Flux<Recipe> getByPortionsGreaterThan(Double portions);

    Flux<Recipe> getByPortionsLessThan(Double portions);

    Flux<Recipe> getByMinNumberOfLikes(int numberOfLikes);

    Flux<Recipe> getByMaxNumberOfLikes(int numberOfLikes);

    Flux<Recipe> getByExactNumberOfLikes(int numberOfLikes);

    Mono<Recipe> save(Recipe recipe);

    Mono<Recipe> update(Recipe recipe);

    Mono<?> delete(String id);

    Mono<Recipe> addLike(String userId, String recipeId);

    Mono<Recipe> removeLike(String userId, String recipeId);
}
