package com.example.cookbookBackend.service;

import com.example.cookbookBackend.exception.RecipeExistsException;
import com.example.cookbookBackend.exception.RecipeNotFoundException;
import com.example.cookbookBackend.model.Recipe;
import com.example.cookbookBackend.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Mono<Recipe> getById(String id) {
        return this.recipeRepository.findById(id);
    }

    @Override
    public Mono<Recipe> getByName(String name) {
        return this.recipeRepository.getRecipeByNameIgnoreCase(name);
    }

    @Override
    public Flux<Recipe> getAll() {
        return this.recipeRepository.findAll();
    }

    @Override
    public Flux<Recipe> getByCreatorId(String creatorId) {
        return this.recipeRepository.getRecipesByCreatorId(creatorId);
    }

    @Override
    public Flux<Recipe> getByIngredientsId(String ingredientId) {
        return this.recipeRepository.getRecipesByIngredients(ingredientId);
    }

    @Override
    public Flux<Recipe> getByPortionsExact(Double portions) {
        return this.recipeRepository.getRecipesByPortionsEquals(portions);
    }

    @Override
    public Flux<Recipe> getByPortionsGreaterThan(Double portions) {
        return this.recipeRepository.getRecipesByPortionsGreaterThan(portions);
    }

    @Override
    public Flux<Recipe> getByPortionsLessThan(Double portions) {
        return this.recipeRepository.getRecipesByPortionsLessThan(portions);
    }

    @Override
    public Flux<Recipe> getByMinNumberOfLikes(int numberOfLikes) {
        return this.recipeRepository.getRecipesByLikedByUserIdsGreaterThan(numberOfLikes);
    }

    @Override
    public Flux<Recipe> getByMaxNumberOfLikes(int numberOfLikes) {
        return this.recipeRepository.getRecipesByLikedByUserIdsLessThan(numberOfLikes);
    }

    @Override
    public Flux<Recipe> getByExactNumberOfLikes(int numberOfLikes) {
        return this.recipeRepository.getRecipesByLikedByUserIdsEquals(numberOfLikes);
    }

    @Override
    public Mono<Recipe> save(Recipe recipe) {
        return this.recipeRepository.save(recipe)
                .onErrorResume(e -> Mono.error(new RecipeExistsException(recipe.getName())));
    }

    @Override
    public Mono<Recipe> update(Recipe recipe) {
        return recipeRepository.findById(recipe.getId())
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.BAD_REQUEST, recipe.getId())))
                .flatMap(recipeToUpdate -> updateFields(recipe, recipeToUpdate))
                .flatMap(this.recipeRepository::save);
    }

    private Mono<Recipe> updateFields(Recipe recipe, Recipe recipeToUpdate) {
        if (recipe.getName() != null) {
            if (!recipe.getName().equals(recipeToUpdate.getName())) {
                recipeToUpdate.setName(recipe.getName());
            }
        }

        if (!recipe.getIngredients().isEmpty()) {
            for (Map.Entry<String, Double> entry: recipe.getIngredients().entrySet()) {
                String key = entry.getKey();
                Double value = entry.getValue();
                Double existingValue = recipeToUpdate.getIngredients().get(key);
                if (existingValue == null || !existingValue.equals(value)) {
                    recipeToUpdate.getIngredients().put(key, value);
                }
            }
        }

        if (recipe.getPortions() != null) {
            if (!recipe.getPortions().equals(recipeToUpdate.getPortions())) {
                recipeToUpdate.setPortions(recipe.getPortions());
            }
        }

        if (recipe.getRecipeCategoryId() != null) {
            if (!recipe.getRecipeCategoryId().equals(recipeToUpdate.getRecipeCategoryId())) {
                recipeToUpdate.setRecipeCategoryId(recipe.getRecipeCategoryId());
            }
        }

        if (recipe.getImage() != null) {
            if (!recipe.getImage().equals(recipeToUpdate.getImage())) {
                recipeToUpdate.setImage(recipe.getImage());
            }
        }

        if (recipe.getDescription() != null) {
            if (!recipe.getDescription().equals(recipeToUpdate.getDescription())) {
                recipeToUpdate.setDescription(recipe.getDescription());
            }
        }

        if (recipe.getCookTimeInMinutes() != null) {
            if (!recipe.getCookTimeInMinutes().equals(recipeToUpdate.getCookTimeInMinutes())) {
                recipeToUpdate.setCookTimeInMinutes(recipe.getCookTimeInMinutes());
            }
        }

        return Mono.just(recipeToUpdate);
    }

    @Override
    public Mono<?> delete(String id) {
        return this.recipeRepository.findById(id)
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.BAD_REQUEST, id)))
                .flatMap(this.recipeRepository::delete);
    }

    @Override
    public Mono<Recipe> addLike(String userId, String recipeId) {
        Mono<Recipe> recipe = this.recipeRepository.findById(recipeId);
        // TODO: implement logic to check userId!
        return recipe
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.BAD_REQUEST, recipeId)))
                .flatMap(r -> {
                    if (!r.getLikedByUserIds().contains(userId)) {
                        r.getLikedByUserIds().add(userId);
                    }
                    return this.recipeRepository.save(r);
                });
    }

    @Override
    public Mono<Recipe> removeLike(String userId, String recipeId) {
        return this.recipeRepository.findById(recipeId)
                .switchIfEmpty(Mono.error(new RecipeNotFoundException(HttpStatus.BAD_REQUEST, recipeId)))
                .flatMap(r -> {
                    r.getLikedByUserIds().remove(userId);
                    return this.recipeRepository.save(r);
                });
    }
}
