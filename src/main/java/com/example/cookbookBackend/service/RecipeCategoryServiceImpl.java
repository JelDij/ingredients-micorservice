package com.example.cookbookBackend.service;

import com.example.cookbookBackend.exception.CategoryExistsException;
import com.example.cookbookBackend.exception.CategoryNotFoundException;
import com.example.cookbookBackend.model.RecipeCategory;
import com.example.cookbookBackend.repository.RecipeCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RecipeCategoryServiceImpl implements RecipeCategoryService {

    private final RecipeCategoryRepository recipeCategoryRepository;

    @Autowired
    public RecipeCategoryServiceImpl(RecipeCategoryRepository recipeCategoryRepository) {
        this.recipeCategoryRepository = recipeCategoryRepository;
    }

    @Override
    public Mono<RecipeCategory> getById(String id) {
        return this.recipeCategoryRepository.findById(id);
    }

    @Override
    public Mono<RecipeCategory> getByName(String name) {
        return this.recipeCategoryRepository.findByNameIgnoreCase(name);
    }

    @Override
    public Flux<RecipeCategory> getAll() {
        return this.recipeCategoryRepository.findAll();
    }

    @Override
    public Mono<RecipeCategory> save(RecipeCategory recipeCategory) {
        return this.recipeCategoryRepository.save(recipeCategory)
                .onErrorResume(e -> Mono.error(new CategoryExistsException(recipeCategory.getName())));
    }

    @Override
    public Mono<RecipeCategory> update(RecipeCategory recipeCategory) {
        return this.recipeCategoryRepository.findById(recipeCategory.getId())
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "id", recipeCategory.getId())))
                .flatMap(categoryToUpdate -> {
                    categoryToUpdate.setName(recipeCategory.getName());
                    return this.recipeCategoryRepository.save(categoryToUpdate);
                });
    }

    @Override
    public Mono<?> delete(String id) {
        return this.recipeCategoryRepository.findById(id)
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "id", id)))
                .flatMap(category -> this.recipeCategoryRepository.deleteById(id));
    }
}
