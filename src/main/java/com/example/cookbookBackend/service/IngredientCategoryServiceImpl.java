package com.example.cookbookBackend.service;

import com.example.cookbookBackend.exception.CategoryExistsException;
import com.example.cookbookBackend.exception.CategoryNotFoundException;
import com.example.cookbookBackend.model.IngredientCategory;
import com.example.cookbookBackend.repository.IngredientCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class IngredientCategoryServiceImpl implements IngredientCategoryService {

    private final IngredientCategoryRepository ingredientCategoryRepository;

    @Autowired
    public IngredientCategoryServiceImpl(IngredientCategoryRepository ingredientCategoryRepository) {
        this.ingredientCategoryRepository = ingredientCategoryRepository;
    }

    @Override
    public Mono<IngredientCategory> getById(String id) {
        return this.ingredientCategoryRepository.findById(id);
    }

    @Override
    public Mono<IngredientCategory> getByName(String name) {
        return this.ingredientCategoryRepository.findByNameIgnoreCase(name);
    }

    @Override
    public Flux<IngredientCategory> getAll() {
        return this.ingredientCategoryRepository.findAll();
    }

    @Override
    public Mono<IngredientCategory> save(IngredientCategory ingredientCategory) {
        return this.ingredientCategoryRepository.save(ingredientCategory)
                .onErrorResume(e -> Mono.error(new CategoryExistsException(ingredientCategory.getName())));
    }

    @Override
    public Mono<IngredientCategory> update(IngredientCategory ingredientCategory) {
        return this.ingredientCategoryRepository.findById(ingredientCategory.getId())
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "id", ingredientCategory.getId())))
                .flatMap(categoryToUpdate -> {
                    categoryToUpdate.setName(ingredientCategory.getName());
                    return this.ingredientCategoryRepository.save(categoryToUpdate);
                });

    }

    @Override
    public Mono<?> delete(String id) {
        return this.ingredientCategoryRepository.findById(id)
                .switchIfEmpty(Mono.error(new CategoryNotFoundException(HttpStatus.BAD_REQUEST, "id", id)))
                .flatMap(category -> this.ingredientCategoryRepository.deleteById(id));
    }
}
