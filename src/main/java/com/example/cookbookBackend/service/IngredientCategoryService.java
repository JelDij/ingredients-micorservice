package com.example.cookbookBackend.service;

import com.example.cookbookBackend.model.IngredientCategory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IngredientCategoryService {

    Mono<IngredientCategory> getById(String id);

    Mono<IngredientCategory> getByName(String name);

    Flux<IngredientCategory> getAll();

    Mono<IngredientCategory> save(IngredientCategory ingredientCategory);

    Mono<IngredientCategory> update(IngredientCategory ingredientCategory);

    Mono<?> delete(String id);
}
