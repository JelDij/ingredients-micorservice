package com.example.cookbookBackend.repository;

import com.example.cookbookBackend.model.RecipeCategory;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface RecipeCategoryRepository extends ReactiveMongoRepository<RecipeCategory, String> {

    Mono<RecipeCategory> findByNameIgnoreCase(String name);
}
