package com.example.cookbookBackend.repository;

import com.example.cookbookBackend.model.Recipe;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RecipeRepository extends ReactiveMongoRepository<Recipe, String> {

    Mono<Recipe> getRecipeByNameIgnoreCase(String name);

    Flux<Recipe> getRecipesByCreatorId(String creatorId);

    Flux<Recipe> getRecipesByIngredients(String ingredientId);

    Flux<Recipe> getRecipesByPortionsEquals(Double portions);

    Flux<Recipe> getRecipesByPortionsGreaterThan(Double portions);

    Flux<Recipe> getRecipesByPortionsLessThan(Double portions);

    Flux<Recipe> getRecipesByLikedByUserIdsGreaterThan(int likes);

    Flux<Recipe> getRecipesByLikedByUserIdsLessThan(int likes);

    Flux<Recipe> getRecipesByLikedByUserIdsEquals(int likes);
}
