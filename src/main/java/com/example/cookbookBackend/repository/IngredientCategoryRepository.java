package com.example.cookbookBackend.repository;

import com.example.cookbookBackend.model.IngredientCategory;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface IngredientCategoryRepository extends ReactiveMongoRepository<IngredientCategory, String> {

    Mono<IngredientCategory> findByNameIgnoreCase(String name);
}
