package com.example.cookbookBackend.repository;

import com.example.cookbookBackend.model.RecipeStep;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface RecipeStepRepository extends ReactiveMongoRepository<RecipeStep, String> {

    Flux<RecipeStep> findRecipeStepsByRecipeId(String recipeId);
}
