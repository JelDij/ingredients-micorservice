package com.example.cookbookBackend.repository;

import com.example.cookbookBackend.model.Ingredient;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface IngredientRepository extends ReactiveMongoRepository<Ingredient, String> {

    Mono<Ingredient> findByNameIgnoreCase(String name);
    Flux<Ingredient> findIngredientByIngredientCategoryId(String ingredientCategoryId);
}
