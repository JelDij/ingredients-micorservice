package com.example.cookbookBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookBookBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CookBookBackendApplication.class, args);
    }

}
