package com.example.cookbookBackend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document("ingredients")
public class Ingredient {

    @Id
    private String id;
    @Indexed(unique = true)
    private String name;
    private String ingredientCategoryId;
    private String unitOfMeasurement;

    public Ingredient() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredientCategoryId() {
        return this.ingredientCategoryId;
    }

    public void setIngredientCategoryId(String ingredientCategoryId) {
        this.ingredientCategoryId = ingredientCategoryId;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", categoryId=" + ingredientCategoryId +
                ", unitOfMeasurement='" + unitOfMeasurement + '\'' +
                '}';
    }
}
